﻿using System;
using System.Net;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace MVC.Controllers
{
    public class TerritoryController : Controller
    {
        // GET: Territory
        public async Task<ActionResult> Index()
        {
            IEnumerable<TerritoryModels> Territorys = new List<TerritoryModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/Territory");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    Territorys = JsonConvert.DeserializeObject<List<TerritoryModels>>(temp);
                }
            }
            return View(Territorys);
        }

        // GET: Territory/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Territory/Create
        public async Task<ActionResult> Create()
        {
            TerritoryModels house = new TerritoryModels();
            List<HouseModels> houses = new List<HouseModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    houses = JsonConvert.DeserializeObject<List<HouseModels>>(temp);
                }
            }
            ViewBag.ListHouses = houses.Select(h => new SelectListItem()
            {
                Text = h.Name,
                Value = h.ID.ToString()
            });
            List<TerritoryType> terr = new List<TerritoryType>();
            terr.Add(TerritoryType.DESERT);
            terr.Add(TerritoryType.LAND);
            terr.Add(TerritoryType.MOUNTAIN);
            terr.Add(TerritoryType.SEA);
            ViewBag.ListType = terr;
            return View(house);
        }

        // POST: Territory/Create
        [HttpPost]
        // POST: House/Edit/5
        public async Task<ActionResult> Create(TerritoryModels collection)
        {

            TerritoryModels house = new TerritoryModels();
            house.Name = collection.Name;
            house.Type = collection.Type;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house/" + collection.Owner.ID);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    house.Owner = JsonConvert.DeserializeObject<HouseModels>(temp);
                }
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                String jsonString = JsonConvert.SerializeObject(house);
                StringContent content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("api/Territory", content);
                return RedirectToAction("Index");
            }
        }

        // GET: Territory/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            TerritoryModels house = new TerritoryModels();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/territory/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    house = JsonConvert.DeserializeObject<TerritoryModels>(temp);
                }
            }

            List<HouseModels> houses = new List<HouseModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    houses = JsonConvert.DeserializeObject<List<HouseModels>>(temp);
                }
            }
            ViewBag.ListHouses = houses.Select(h => new SelectListItem()
            {
                Text = h.Name,
                Value = h.ID.ToString()
            });
            List<TerritoryType> terr = new List<TerritoryType>();
            terr.Add(TerritoryType.DESERT);
            terr.Add(TerritoryType.LAND);
            terr.Add(TerritoryType.MOUNTAIN);
            terr.Add(TerritoryType.SEA);
            ViewBag.ListType = terr;
            return View(house);
        }

        [HttpPost]
        // POST: House/Edit/5
        public async Task<ActionResult> Edit(int id, TerritoryModels collection)
        {

            TerritoryModels house = new TerritoryModels();
            house.ID = id;
            house.Name = collection.Name;
            house.Type = collection.Type;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house/" + collection.Owner.ID);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    house.Owner = JsonConvert.DeserializeObject<HouseModels>(temp);
                }
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                String jsonString = JsonConvert.SerializeObject(house);
                StringContent content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync("api/Territory", content);
                return RedirectToAction("Index");
            }
        }

        // GET: Territory/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            TerritoryModels character = new TerritoryModels();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/Territory/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    character = JsonConvert.DeserializeObject<TerritoryModels>(temp);
                }
            }
            return View(character);
        }

        // POST: Fight/Delete/2
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:13666/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.DeleteAsync("api/Territory/" + id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
