﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public enum TerritoryType
    {
        SEA,
        MOUNTAIN,
        LAND,
        DESERT
    }
    public class TerritoryModels
    {
        public int ID { get; set; }
        public TerritoryType Type { get; set; }
        public HouseModels Owner { get; set; }
        public string Name { get; set; }
        public TerritoryModels()
        {
            ID = 0;
            Type = 0;
            Owner = null;
            Name = "";
        }
    }
}