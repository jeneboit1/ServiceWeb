﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class HouseModels
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int NbUnits { get; set; }

        public HouseModels()
        {
            ID = 0;
            Name = "";
            NbUnits = 0;
        }
    }
}