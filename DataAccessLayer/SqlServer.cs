﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace DataAccessLayer
{
    class SqlServer : DataBaseBridge
    {
        //private string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=H:\\ServiceWeb\\Database\\db.mdf;Integrated Security=True;Connect Timeout=30";
        //private string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\NEBOIT\\Documents\\GitHub\\ServiceWeb\\Database\\db.mdf;Integrated Security=True;Connect Timeout=30";
        private string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\JULES\\DOCUMENTS\\ISIMA\\2ÈME ANNÉE\\SERVICEWEB\\DATABASE\\DB.MDF;Integrated Security=True;Connect Timeout=30";

        private void LogException(Exception e)
        {
            Console.WriteLine("Exception: " + e.Message + ", Source" + e.Source);
        }

        private DataTable SelectByDataAdapter(string request)
        {
            DataTable results = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(request, sqlConnection);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(results);
                }
                catch (SqlException e)
                {
                    LogException(e);
                }
            }
            return results;
        }

        private int UpdateByCommandBuilder(string request, DataTable table)
        {
            int result = 0;
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(request, sqlConnection);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                    SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);

                    sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();
                    sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                    sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();

                    sqlDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

                    result = sqlDataAdapter.Update(table);
                }
                catch (Exception e)
                {
                    LogException(e);
                }
            }
            return result;
        }

        public List<RelationShip> RelationShipList(int id)
        {
            List<RelationShip> res = new List<RelationShip>();
            try
            {
                DataTable dataTable = SelectByDataAdapter("Select * From Relationship Where id_charach1 = " + id + " OR id_charach2 = " + id);

                foreach (DataRow row in dataTable.Rows)
                {
                    int id_character = Convert.ToInt32(row["id_charach1"]);
                    if (id_character == id)
                    {
                        id_character = Convert.ToInt32(row["id_charach2"]);
                    }
                    Character character = getCharacterById(id_character);
                    res.Add(new RelationShip(character, (RelationshipType)Convert.ToInt32(row["type"])));
                }
            }
            catch (Exception e)
            {
                LogException(e);
            }
            return res;
        }

        public List<Character> CharactersList()
        {
            List<Character> res = new List<Character>();
            try
            {
                DataTable dataTable = SelectByDataAdapter("Select * From Character");

                foreach (DataRow row in dataTable.Rows)
                {
                    int id_house = Convert.ToInt32(row["id_house"]);
                    House house = getHouseById(id_house);
                    Character character = new Character(Convert.ToInt32(row["Id"]), row["FirstName"].ToString(), row["LastName"].ToString(), Convert.ToUInt32(row["PV"]), (CharacterType)Convert.ToInt32(row["Type"]), Convert.ToInt32(row["Bravoury"]), Convert.ToInt32(row["Crazyness"]), house, RelationShipList(Convert.ToInt32(row["Id"])));
                    res.Add(character);
                }
            }
            catch(Exception e)
            {
                LogException(e);
            }
            return res;
        }

        public List<Characteristics> CharactersCharacteristicsList()
        {
            List<Characteristics> res = new List<Characteristics>();
            try
            {
                DataTable dataTable = SelectByDataAdapter("Select * From Character");

                foreach (DataRow row in dataTable.Rows)
                {
                    Characteristics characteristics = new Characteristics(Convert.ToUInt32(row["PV"]), (CharacterType)Convert.ToInt32(row["Type"]), Convert.ToInt32(row["Bravoury"]), Convert.ToInt32(row["Crazyness"]));
                    res.Add(characteristics);
                }
            }
            catch (Exception e)
            {
                LogException(e);
            }
            return res;
        }


        public List<Fight> FightsList()
        {
            List<Fight> res = new List<Fight>();
            try
            {
                DataTable dataTable = SelectByDataAdapter("Select * From Fight");

                foreach (DataRow row in dataTable.Rows)
                {
                    int id_house = Convert.ToInt32(row["id_challenger1"]);
                    House chal1 = getHouseById(id_house);
                    id_house = Convert.ToInt32(row["id_challenger2"]);
                    House chal2 = getHouseById(id_house);

                    int id_war = (row["id_war"] is DBNull) ? -1 : Convert.ToInt32(row["id_war"]);
                    War war = (id_war == -1) ? null : getWarById(id_war);

                    int id_winner = (row["id_winner"] is DBNull) ? -1 : Convert.ToInt32(row["id_winner"]);
                    House winner = (id_winner == -1) ? null : getHouseById(id_winner);

                    int id_territory = Convert.ToInt32(row["id_territory"]);
                    Territory territory = getTerritoryById(id_territory);

                    Fight fight = new Fight(Convert.ToInt32(row["Id"]), row["name"].ToString(), chal1, chal2, winner, war, territory);
                    res.Add(fight);
                }
            }
            catch (Exception e)
            {
                LogException(e);
            }
            return res;
        }

        public List<House> HousesList()
        {
            List<House> res = new List<House>();
            DataTable dataTable = SelectByDataAdapter("Select * From House");
            foreach (DataRow row in dataTable.Rows)
            {
                House house = new House(Convert.ToInt32(row["Id"]), row["name"].ToString(), Convert.ToInt32(row["nbUnits"]));
                res.Add(house);
            }
            return res;
        }

        public List<House> HousesList(int sup)
        {
            List<House> res = new List<House>();
            DataTable dataTable = SelectByDataAdapter("Select * From House Where nbUnits >" + sup);
            foreach (DataRow row in dataTable.Rows)
            {
                House house = new House(Convert.ToInt32(row["Id"]), row["name"].ToString(), Convert.ToInt32(row["nbUnits"]));
                res.Add(house);
            }
            return res;
        }

        public List<War> WarList()
        {
            List<War> res = new List<War>();
            DataTable dataTable = SelectByDataAdapter("Select * From War");
            foreach (DataRow row in dataTable.Rows)
            {
                War war = new War(Convert.ToInt32(row["Id"]), row["name"].ToString());
                res.Add(war);
            }
            return res;
        }

        public House getHouseById(int id)
        {
            House res = null;
            try
            {
                DataTable houseData = SelectByDataAdapter("Select * From House Where Id=" + id);
                DataRow houseRow = houseData.Rows[0];
                res = new House(Convert.ToInt32(houseRow["Id"]), houseRow["name"].ToString(), Convert.ToInt32(houseRow["nbUnits"]));
            }
            catch (Exception e)
            {
                LogException(e);
            }
            return res;
        }

        public Character getCharacterById(int id)
        {
            Character res = null;
            try
            {
                DataTable characData = SelectByDataAdapter("Select * From Character Where Id=" + id);
                DataRow characRow = characData.Rows[0];
                int id_house = Convert.ToInt32(characRow["id_house"]);
                House house = getHouseById(id_house);
                res = new Character(Convert.ToInt32(characRow["Id"]), characRow["FirstName"].ToString(), characRow["LastName"].ToString(), Convert.ToUInt32(characRow["PV"]), (CharacterType)Convert.ToInt32(characRow["Type"]), Convert.ToInt32(characRow["Bravoury"]), Convert.ToInt32(characRow["Crazyness"]), house);
            }
            catch (Exception e)
            {
                LogException(e);
            }

            return res;
        }

        public War getWarById(int id)
        {
            War res = null;
            try
            {
                DataTable warData = SelectByDataAdapter("Select * From War Where Id=" + id);
                DataRow warRow = warData.Rows[0];
                res = new War(Convert.ToInt32(warRow["Id"]), warRow["name"].ToString());
            }
            catch (Exception e)
            {
                LogException(e);
            }

            return res;
        }

        public Territory getTerritoryById(int id)
        {
            Territory res = null;
            try
            {
                DataTable territoryData = SelectByDataAdapter("Select * From Territory Where Id=" + id);
                DataRow territoryRow = territoryData.Rows[0];
                int id_house = Convert.ToInt32(territoryRow["id_house"]);
                House house = getHouseById(id_house);
                res = new Territory(Convert.ToInt32(territoryRow["Id"]), (TerritoryType)territoryRow["type"], house, territoryRow["name"].ToString());
            }
            catch (Exception e)
            {
                LogException(e);
            }
            return res;
        }

        public Fight getFightById(int id)
        {
            Fight res = null;
            try
            {
                DataTable fightData = SelectByDataAdapter("Select * From Fight Where Id=" + id);
                DataRow fightRow = fightData.Rows[0];
                int id_house1 = Convert.ToInt32(fightRow["id_challenger1"]);
                House house1 = getHouseById(id_house1);
                int id_house2 = Convert.ToInt32(fightRow["id_challenger2"]);
                House house2 = getHouseById(id_house2);
                int id_winner = Convert.ToInt32(fightRow["id_winner"]);
                int id_war = Convert.ToInt32(fightRow["id_war"]);
                War war = getWarById(id_war);
                int id_territory = Convert.ToInt32(fightRow["id_territory"]);
                Territory territory = getTerritoryById(id_territory);
                res = new Fight(id, fightRow["name"].ToString(), house1, house2, (id_winner == house1.ID) ? house1 : house2, war, territory);
            }
            catch (Exception e)
            {
                LogException(e);
            }
            return res;
        }

        public List<Fight> getFightbyWarID(int id)
        {
            List<Fight> res = new List<Fight>();
            try
            {
                DataTable dataTable = SelectByDataAdapter("Select * From Fight Where id_war = " + id);

                foreach (DataRow row in dataTable.Rows)
                {
                    int id_house = Convert.ToInt32(row["id_challenger1"]);
                    House chal1 = getHouseById(id_house);
                    id_house = Convert.ToInt32(row["id_challenger2"]);
                    House chal2 = getHouseById(id_house);

                    int id_war = (row["id_war"] is DBNull) ? -1 : Convert.ToInt32(row["id_war"]);
                    War war = (id_war == -1) ? null : getWarById(id_war);

                    int id_winner = (row["id_winner"] is DBNull) ? -1 : Convert.ToInt32(row["id_winner"]);
                    House winner = (id_winner == -1) ? null : getHouseById(id_winner);

                    int id_territory = Convert.ToInt32(row["id_territory"]);
                    Territory territory = getTerritoryById(id_territory);

                    Fight fight = new Fight(Convert.ToInt32(row["Id"]), row["name"].ToString(), chal1, chal2, winner, war, territory);
                    res.Add(fight);
                }
            }
            catch (Exception e)
            {
                LogException(e);
            }
            return res;
        }

        public List<Territory> TerritoriesList()
        {
            List<Territory> res = new List<Territory>();

            try
            {
                DataTable dataTable = SelectByDataAdapter("Select * From Territory");

                foreach (DataRow row in dataTable.Rows)
                {
                    int id_house = Convert.ToInt32(row["id_house"]);
                    House owner = getHouseById(id_house);
                    Territory territory = new Territory(Convert.ToInt32(row["Id"]), (TerritoryType)Convert.ToInt32(row["type"]), owner, row["name"].ToString());
                    res.Add(territory);
                }
            }
            catch (Exception e)
            {
                LogException(e);
            }

            return res;
        }

        public int addCharacter(Character character)
        {
            DataTable characData = SelectByDataAdapter("INSERT INTO Character VALUES('" + character.FirstName + "','" + character.LastName + "'," + character.Characteristics.PV + "," + (int)character.Characteristics.Type + "," + character.Characteristics.Bravoury + "," + character.Characteristics.Crazyness + "," + character.House.ID + "); SELECT SCOPE_IDENTITY() AS ID");
            return (characData.Rows.Count == 1) ? Convert.ToInt32(characData.Rows[0]["ID"]) : -1;
        }

        public void deleteCharacter(int id)
        {
            SelectByDataAdapter("DELETE FROM Character Where Id=" + id);
        }

        public void updateCharacter(Character character)
        {
            String request = "Select * From Character Where Id=" + character.ID;
            DataTable dataTable = SelectByDataAdapter(request);
            if (dataTable.Rows.Count == 1)
            {
                DataRow row = dataTable.Rows[0];
                row["FirstName"] = character.FirstName;
                row["LastName"] = character.LastName;
                row["PV"] = character.Characteristics.PV;
                row["Type"] = (int)character.Characteristics.Type;
                row["Bravoury"] = character.Characteristics.Bravoury;
                row["Crazyness"] = character.Characteristics.Crazyness;
                row["id_house"] = character.House.ID;
                UpdateByCommandBuilder(request, dataTable);
            }
        }

        public int addHouse(House house)
        {
            DataTable houseData = SelectByDataAdapter("INSERT INTO House VALUES('" + house.Name + "'," + house.NbUnits + "); SELECT SCOPE_IDENTITY() AS ID");
            return (houseData.Rows.Count == 1) ? Convert.ToInt32(houseData.Rows[0]["ID"]) : -1;
        }

        public void deleteHouse(int id)
        {
            SelectByDataAdapter("DELETE FROM House Where Id=" + id);
        }

        public void updateHouse(House house)
        {
            String request = "Select * From House Where Id=" + house.ID;
            DataTable dataTable = SelectByDataAdapter(request);
            if (dataTable.Rows.Count == 1)
            {
                dataTable.Rows[0]["name"] = house.Name;
                dataTable.Rows[0]["nbUnits"] = house.NbUnits;
                UpdateByCommandBuilder(request, dataTable);
            }
        }
        public int addFight(Fight fight)
        {
            DataTable fightData = SelectByDataAdapter("INSERT INTO Fight VALUES('" + fight.Name + "'," + fight.Challenger1.ID + "," + fight.Challenger2.ID + "," + fight.Winner.ID + "," + fight.War.ID + "," + fight.Territory.ID + "); SELECT SCOPE_IDENTITY() AS ID");
            return (fightData.Rows.Count == 1) ? Convert.ToInt32(fightData.Rows[0]["ID"]) : -1;
        }

        public void deleteFight(int id)
        {
            SelectByDataAdapter("DELETE FROM Fight Where Id=" + id);
        }

        public void updateFight(Fight fight)
        {
            String request = "Select * From Fight Where Id=" + fight.ID;
            DataTable dataTable = SelectByDataAdapter(request);
            if (dataTable.Rows.Count == 1)
            {
                dataTable.Rows[0]["name"] = fight.Name;
                dataTable.Rows[0]["id_challenger1"] = fight.Challenger1.ID;
                dataTable.Rows[0]["id_challenger2"] = fight.Challenger2.ID;
                dataTable.Rows[0]["id_winner"] = fight.Winner.ID;
                dataTable.Rows[0]["id_war"] = fight.War.ID;
                dataTable.Rows[0]["id_territory"] = fight.Territory.ID;
                UpdateByCommandBuilder(request, dataTable);
            }
        }
        public int addTerritory(Territory territory)
        {
            DataTable territoryData = SelectByDataAdapter("INSERT INTO Territory VALUES(" + (int)territory.Type + "," + territory.Owner.ID + ",'" + territory.Name + "'); SELECT SCOPE_IDENTITY() AS ID");
            return (territoryData.Rows.Count == 1) ? Convert.ToInt32(territoryData.Rows[0]["ID"]) : -1;
        }

        public void deleteTerritory(int id)
        {
            SelectByDataAdapter("DELETE FROM Territory Where Id=" + id);
        }

        public void updateTerritory(Territory territory)
        {
            String request = "Select * From Territory Where Id=" + territory.ID;
            DataTable dataTable = SelectByDataAdapter(request);
            if (dataTable.Rows.Count == 1)
            {
                dataTable.Rows[0]["type"] = territory.Type;
                dataTable.Rows[0]["id_house"] = territory.Owner.ID;
                dataTable.Rows[0]["name"] = territory.Name;
                UpdateByCommandBuilder(request, dataTable);
            }
        }
        public int addWar(War war)
        {
            DataTable warData = SelectByDataAdapter("INSERT INTO War VALUES('" + war.Name + "'" +  "); SELECT SCOPE_IDENTITY() AS ID");
            return (warData.Rows.Count == 1) ? Convert.ToInt32(warData.Rows[0]["ID"]) : -1;
        }

        public void deleteWar(int id)
        {
            SelectByDataAdapter("DELETE FROM War Where Id=" + id);
        }

        public void updateWar(War war)
        {
            String request = "Select * From War Where Id=" + war.ID;
            DataTable dataTable = SelectByDataAdapter(request);
            if (dataTable.Rows.Count == 1)
            {
                dataTable.Rows[0]["name"] = war.Name;
                UpdateByCommandBuilder(request, dataTable);
            }
        }
    }
}
