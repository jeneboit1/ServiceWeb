﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    interface DataBaseBridge
    {
        List<House> HousesList();
        List<House> HousesList(int sup);
        List<RelationShip> RelationShipList(int id);
        List<Character> CharactersList();
        List<Characteristics> CharactersCharacteristicsList();
        List<Territory> TerritoriesList();
        List<Fight> FightsList();
        List<War> WarList();

        Character getCharacterById(int id);
        House getHouseById(int id);
        War getWarById(int id);
        Territory getTerritoryById(int id);
        Fight getFightById(int id);
        List<Fight> getFightbyWarID(int id);

        int addCharacter(Character character);
        void deleteCharacter(int id);
        void updateCharacter(Character character);

        int addHouse(House house);
        void deleteHouse(int id);
        void updateHouse(House house);

        int addFight(Fight house);
        void deleteFight(int id);
        void updateFight(Fight fight);

        int addTerritory(Territory territory);
        void deleteTerritory(int id);
        void updateTerritory(Territory territory);

        int addWar(War war);
        void deleteWar(int id);
        void updateWar(War war);
    }
}
