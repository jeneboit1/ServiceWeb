﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DalManager
    {
        private static DalManager _instance;
        private static readonly object padlock = new object();

        private DataBaseBridge dataBaseBridge;

        private DalManager()
        {
            dataBaseBridge = new SqlServer();
        }

        public static DalManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (padlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new DalManager();
                        }
                    }
                }
                return _instance;
            }
        }

        public List<House> HousesList()
        {
            return dataBaseBridge.HousesList();
        }

        public List<House> HousesList(int sup)
        {
            return dataBaseBridge.HousesList(sup);
        }

        public List<Fight> FightsList()
        {
            return dataBaseBridge.FightsList();
        }

        public Character getCharacterById(int id)
        {
            return dataBaseBridge.getCharacterById(id);
        }

        public House getHouseById(int id)
        {
            return dataBaseBridge.getHouseById(id);
        }

        public War getWarById(int id)
        {
            return dataBaseBridge.getWarById(id);
        }

        public Territory getTerritoryById(int id)
        {
            return dataBaseBridge.getTerritoryById(id);
        }

        public Fight getFightById(int id)
        {
            return dataBaseBridge.getFightById(id);
        }

        public List<Fight> getFightbyWarID(int id)
        {
            return dataBaseBridge.getFightbyWarID(id);
        }
        public List<Characteristics> CharactersCharacteristicsList()
        {
            return dataBaseBridge.CharactersCharacteristicsList();
        }

        public List<Character> CharactersList()
        {
            return dataBaseBridge.CharactersList();
        }

        public List<Territory> TerritoriesList()
        {
            return dataBaseBridge.TerritoriesList();
        }

        public List<War> WarList()
        {
            return dataBaseBridge.WarList();
        }

        public int addCharacter(Character character)
        {
            return dataBaseBridge.addCharacter(character);
        }

        public void deleteCharacter(int id)
        {
            dataBaseBridge.deleteCharacter(id);
        }

        public void updateCharacter(Character character)
        {
            dataBaseBridge.updateCharacter(character);
        }

        public int addHouse(House house)
        {
            return dataBaseBridge.addHouse(house);
        }

        public void deleteHouse(int id)
        {
            dataBaseBridge.deleteHouse(id);
        }

        public void updateHouse(House house)
        {
            dataBaseBridge.updateHouse(house);
        }

        public int addFight(Fight fight)
        {
            return dataBaseBridge.addFight(fight);
        }

        public void deleteFight(int id)
        {
            dataBaseBridge.deleteFight(id);
        }

        public void updateFight(Fight fight)
        {
            dataBaseBridge.updateFight(fight);
        }

        public int addTerritory(Territory territory)
        {
            return dataBaseBridge.addTerritory(territory);
        }

        public void deleteTerritory(int id)
        {
            dataBaseBridge.deleteTerritory(id);
        }

        public void updateTerritory(Territory territory)
        {
            dataBaseBridge.updateTerritory(territory);
        }

        public int addWar(War war)
        {
            return dataBaseBridge.addWar(war);
        }

        public void deleteWar(int id)
        {
            dataBaseBridge.deleteWar(id);
        }

        public void updateWar(War war)
        {
            dataBaseBridge.updateWar(war);
        }
    }
}
