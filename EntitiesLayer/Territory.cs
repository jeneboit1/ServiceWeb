﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public enum TerritoryType
    {
        SEA,
        MOUNTAIN,
        LAND,
        DESERT
    }
    public class Territory : EntityObject
    {
        public TerritoryType Type { get; set; }
        public House Owner { get; set; }
        public string Name { get; set; }
        public Territory(TerritoryType type, House owner, string name)
        {
            ID = 0;
            Type = type;
            Owner = owner;
            Name = name;
        }

        public Territory(int id, TerritoryType type, House owner,string name)
        {
            ID = id;
            Type = type;
            Owner = owner;
            Name = name;
        }
    }
}
