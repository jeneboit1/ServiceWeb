﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public enum RelationshipType
    {
        FRIENDSHIP,
        LOVE,
        HATRED,
        RIVALRY
    }

    public class RelationShip
    {
        public Character Character { get; set; }
        public RelationshipType Type { get; set; }

        public RelationShip(Character character, RelationshipType type)
        {
            Character = character;
            Type = type;
        }
    }
}
