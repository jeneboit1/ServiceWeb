﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class War : EntityObject
    {
        public String Name { get; set; }

        public War(String name)
        {
            ID = 0;
            Name = name;
        }

        public War(int id, String name)
        {
            ID = id;
            Name = name;
        }
    }
}
