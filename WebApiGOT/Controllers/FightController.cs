﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApiGOT.Models;
using BusinessLayer;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EntitiesLayer;

namespace WebApiGOT.Controllers
{
    public class FightController : ApiController
    {
        // GET: api/Fight
        public List<FightDTO> GetAllFight()
        {
            List<FightDTO> list = new List<FightDTO>();
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            foreach (EntitiesLayer.Fight fight in busi.FightsList())
            {
                list.Add(new FightDTO(fight));
            }
            return list;
        }

        // GET: api/Fight/2
        public FightDTO Get(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            return new FightDTO(busi.getFightById(id));
        }

        [Route("api/war/fight/{id}")]
        public List<FightDTO> GetFightByWarID(int id)
        {
            List<FightDTO> list = new List<FightDTO>();
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();

            foreach (EntitiesLayer.Fight fight in busi.getFightbyWarID(id))
            {
                list.Add(new FightDTO(fight));
            }
            return list;
        }

        // POST: api/Fight
        public FightDTO PostAddFight(FightDTO f)
        {
            House challenger1 = new House(f.Challenger1.ID, f.Challenger1.Name, f.Challenger1.NbUnits);
            House challenger2 = new House(f.Challenger2.ID, f.Challenger2.Name, f.Challenger2.NbUnits);
            War war = new War(f.War.ID,f.War.Name);
            Fight fight = new Fight(f.Name, challenger1, challenger2, war, new Territory(f.Territory.ID, f.Territory.Type, new House(f.Territory.Owner.ID, f.Territory.Owner.Name, f.Territory.Owner.NbUnits), f.Territory.Name));
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            int id = busi.addFight(fight);
            FightDTO _fight = new FightDTO(busi.getFightById(id));
            return _fight;
        }

        // PUT: api/Fight/2
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Fight/2
        public void DeleteFight(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.deleteFight(id);
        }
    }
}
