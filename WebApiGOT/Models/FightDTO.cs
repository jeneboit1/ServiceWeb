﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiGOT.Models
{
    public class FightDTO
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public HouseDTO Challenger1 { get; set; }
        public HouseDTO Challenger2 { get; set; }
        public HouseDTO Winner { get; set; }
        public WarDTO War { get; set; }
        public TerritoryDTO Territory { get; set; }

        public FightDTO()
        {
            ID = 0;
            Name = "";
            Challenger1 = null;
            Challenger2 = null;
            Winner = null;
            War = null ;
            Territory = null;
        }
        public FightDTO(Fight f)
        {
            ID = f.ID;
            Name = f.Name;
            Challenger1 = new HouseDTO(f.Challenger1);
            Challenger2 = new HouseDTO(f.Challenger2);
            Winner = (f.Winner == null) ? null : new HouseDTO(f.Winner);
            War = new WarDTO(f.War);
            Territory = new TerritoryDTO(f.Territory);
        }
    }
}