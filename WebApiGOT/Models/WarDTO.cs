﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiGOT.Models
{
    public class WarDTO
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public WarDTO()
        {
            ID = 0;
            Name = "";
        }
        public WarDTO(War w)
        {
            ID = w.ID;
            Name = w.Name;
        }
    }
}