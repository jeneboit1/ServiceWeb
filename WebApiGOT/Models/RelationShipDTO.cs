﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiGOT.Models
{
    public class RelationShipDTO
    {
        public CharacterDTO Character { get; set; }
        public RelationshipType Type { get; set; }

        public RelationShipDTO(CharacterDTO character, RelationshipType type)
        {
            Character = character;
            Type = type;
        }
    }
}