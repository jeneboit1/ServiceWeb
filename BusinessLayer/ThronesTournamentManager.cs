﻿using DataAccessLayer;
using EntitiesLayer;
// using StubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ThronesTournamentManager
    {
        public DalManager DalManager { get; }

        public ThronesTournamentManager()
        {
            //DalManager = new DalManager(); // StubDataAccessLayer
            DalManager = DalManager.Instance;
        }

        public List<House> HousesList()
        {
            return DalManager.HousesList();
        }

        public List<House> HousesList(int sup)
        {
            return DalManager.HousesList(sup);
        }

        public List<Fight> FightsList()
        {
            return DalManager.FightsList();
        }

        public List<War> WarList()
        {
            return DalManager.WarList();
        }

        public Character getCharacterById(int id)
        {
            return DalManager.getCharacterById(id);
        }

        public House getHouseById(int id)
        {
            return DalManager.getHouseById(id);
        }

        public War getWarById(int id)
        {
            return DalManager.getWarById(id);
        }

        public Territory getTerritoryById(int id)
        {
            return DalManager.getTerritoryById(id);
        }

        public Fight getFightById(int id)
        {
            return DalManager.getFightById(id);
        }

        public List<Fight> getFightbyWarID(int id)
        {
            return DalManager.getFightbyWarID(id);
        }

        public List<Characteristics> CharactersCharacteristicsList()
        {
            return DalManager.CharactersCharacteristicsList();
        }

        public List<Character> CharactersList()
        {
            return DalManager.CharactersList();
        }

        public List<Character> CharactersList(int BravourySup, int PVSup)
        {
            return DalManager.CharactersList().Where(c => c.Characteristics.Bravoury > BravourySup && c.Characteristics.PV > PVSup).ToList();
        }

        public List<Territory> TerritoryList()
        {
            return DalManager.TerritoriesList();
        }

        public int addCharacter(Character c)
        {
            return DalManager.addCharacter(c);
        }

        public void deleteCharacater(int id)
        {
            DalManager.deleteCharacter(id);
        }

        public void updateCharacter(Character c)
        {
            DalManager.updateCharacter(c);
        }

        public int addHouse(House h)
        {
            return DalManager.addHouse(h);
        }

        public void deleteHouse(int id)
        {
            DalManager.deleteHouse(id);
        }

        public void updateHouse(House h)
        {
            DalManager.updateHouse(h);
        }

        public int addFight(Fight f)
        {
            return DalManager.addFight(f);
        }

        public void deleteFight(int id)
        {
            DalManager.deleteFight(id);
        }

        public void updateFight(Fight f)
        {
            DalManager.updateFight(f);
        }

        public int addTerritory(Territory t)
        {
            return DalManager.addTerritory(t);
        }

        public void deleteTerritory(int id)
        {
            DalManager.deleteTerritory(id);
        }

        public void updateTerritory(Territory t)
        {
            DalManager.updateTerritory(t);
        }

        public int addWar(War w)
        {
            return DalManager.addWar(w);
        }

        public void deleteWar(int id)
        {
            DalManager.deleteWar(id);
        }

        public void updateWar(War w)
        {
            DalManager.updateWar(w);
        }
    }
}
